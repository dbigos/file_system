#include "Filesystem.h"

struct superblock S_BLOCK;
struct FAT FILE_TABLE[NR_OF_FILES];
char cleanByte;

int create_vd(int given_size)
{
    FILE *FS = NULL;
 	int i;
    given_size *= 1024;
   
    if(given_size <= (FIRST_FREE_SPACE + MINIMUM_FREE_SPACE))
    {
        printf("Too small disc size!\n");
        return -1;
    }

    if(!(FS = fopen("vdisc", "w")))
    {
        printf("Cannot create File System\n");
        return -1;
    }


    S_BLOCK.size = given_size;
    S_BLOCK.files_count = 0;
    S_BLOCK.free_addr = FIRST_FREE_SPACE;
    S_BLOCK.free_space = (given_size - FIRST_FREE_SPACE);
    S_BLOCK.first_to_be_used_index = 0;


    if(!fwrite(&S_BLOCK, sizeof(S_BLOCK), 1, FS))
    {
        printf("Cannot configure FS\n");
        fclose(FS);
        return -1;
    }

    if(!fwrite(FILE_TABLE, sizeof(FILE_TABLE), 1, FS))
    {
        printf("Cannot configure FS\n");
        fclose(FS);
        return -1;
    }
    for(i = sizeof(FILE_TABLE) + sizeof(S_BLOCK); i < given_size; ++i)
    {
        if (!fwrite(&cleanByte, sizeof(cleanByte), 1, FS))
        {
            printf("Cannot configure FS\n");
            fclose(FS);
            return -1;
        }
    }
    fclose(FS);

    printf("File system created\n");
    return 0;
}

int ls()
{
    FILE *FS = NULL;
    int i;

    if(!(FS = fopen("vdisc", "r")))
    {
        printf("Cannot open disc file\n");
        return -1;
    }

    if(fseek(FS, sizeof(S_BLOCK), SEEK_SET) != 0)
    {
        printf("FSEEK failed\n");
        fclose(FS);
        return -1;
    }

    if(fread(FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
    {
        printf("Cannot read FAT\n");
        fclose(FS);
        return -1;
    }
    for(i = 0; i < NR_OF_FILES; ++i)
    {
        if(FILE_TABLE[i].in_use)
            printf("%s\n", FILE_TABLE[i].filename);
    }

    fclose(FS);

    return 0;
}

int statistics()
{
    FILE *FS = NULL;

    if(!(FS = fopen("vdisc", "r")))
    {
        printf("Cannot read disc file\n");
        fclose(FS);
        return -1;
    }

    rewind(FS);

    if(fread(&S_BLOCK, sizeof(S_BLOCK), 1, FS) != 1)
    {
        printf("Cannot read superblock\n");
        fclose(FS);
        return -1;
    }

    if(fread(FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
    {
        printf("Cannot read FAT\n");
        fclose(FS);
        return -1;
    }

    printf("VIRTUAL DISC:\t size: %d B\n",S_BLOCK.size);
    printf("SUPERBLOCK\t size: %d B\n", sizeof(S_BLOCK));
    printf("FAT\t\t size: %lu B, number of files: %d\n", sizeof(FILE_TABLE), S_BLOCK.files_count);
    printf("DATA SEGMENT\t size: %d B, free space: %d\n",(S_BLOCK.size - FIRST_FREE_SPACE), S_BLOCK.free_space);

    fclose(FS);

    return 0;
}

int remove_vd()
{
	printf("File system removed\n");
    return remove("vdisc");
}

int remove_file(const char* name)
{
    int i, j, mem_to_copy;
    FILE *FS = NULL;
    char *buffer;
    int to_move;

    FS = fopen("vdisc", "r+w");
    if(!FS)
    {
        printf("Cannot open disc file\n");
        return -1;
    }

    if(fread(&S_BLOCK, sizeof(S_BLOCK), 1, FS) != 1)
    {
        printf("Cannot read superblock\n");
        fclose(FS);
        return -1;
    }

    if(fread(FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
    {
        printf("Cannot read FAT\n");
        fclose(FS);
        return -1;
    }

    for(i = 0; i < S_BLOCK.first_to_be_used_index; ++i)
    {
        if((!strcmp(FILE_TABLE[i].filename, name)) && (FILE_TABLE[i].in_use == 1))
        {
            mem_to_copy = S_BLOCK.free_addr - FILE_TABLE[i+1].offset;
            buffer = (char*)malloc(mem_to_copy);

            S_BLOCK.free_space += FILE_TABLE[i].f_size;

            to_move = FILE_TABLE[i].f_size;

            --S_BLOCK.files_count;
            S_BLOCK.free_addr -= FILE_TABLE[i].f_size;

            if(fseek(FS, FILE_TABLE[i+1].offset, SEEK_SET) != 0)
            {
                printf("FSEEK failed\n");
                fclose(FS);
                return -1;
            }

            if(fread(buffer, mem_to_copy, 1, FS) != 1)
            {
                printf("Cannot read FAT\n");
                fclose(FS);
                return -1;
            }

            if(fseek(FS, FILE_TABLE[i].offset, SEEK_SET) != 0)
            {
                printf("FSEEK failed1\n");
                fclose(FS);
                return -1;
            }

            if(fwrite(buffer, mem_to_copy, 1, FS) != 1)
            {
                printf("Cannot make defragmentation\n");
                fclose(FS);
                return -1;
            }

            for(j = i+1; j-1 < S_BLOCK.first_to_be_used_index; ++j)
            {

                FILE_TABLE[j].offset -= to_move;
                FILE_TABLE[j-1] = FILE_TABLE[j];
                FILE_TABLE[j].in_use = 0;
            }

            --S_BLOCK.first_to_be_used_index;
            rewind(FS);

            if(fwrite(&S_BLOCK, sizeof(S_BLOCK), 1, FS) != 1)
            {
                printf("Cannot make defragmentation1\n");
                fclose(FS);
                return -1;
            }

            if(fwrite(FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
            {
                printf("Cannot make defragmentation1\n");
                fclose(FS);
                return -1;

            }

            free(buffer);

            printf("%s removed\n", name);
            fclose(FS);
            
            return 0;
        }

    }

    printf("%s not found\n", name);
    fclose(FS);
    return -1;

}

int copy_to_minix(const char* name)
{
    FILE *FS = NULL, *file = NULL;
    int i, addr, size;
    char *buffer;
    file = fopen(name, "r");

    if(file)
    {
        printf("File already exist\n");
        fclose(file);
        return -1;
    }

    FS = fopen("vdisc", "r");

    if(!FS)
    {
        printf("Cannot open disc file\n");
        return -1;
    }

    rewind(FS);

    if(fread(&S_BLOCK, sizeof(S_BLOCK), 1, FS) != 1)
    {
        printf("Cannot read supreblock\n");
        fclose(FS);
        return -1;
    }

    if(fread(&FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
    {
        printf("Cannot read file table\n");
        fclose(FS);
        return -1;
    }

    for(i = 0; i < NR_OF_FILES; ++i)
    {
        if((strcmp(name, FILE_TABLE[i].filename) == 0) && (FILE_TABLE[i].in_use == 1))
        {
            addr = FILE_TABLE[i].offset;
            size = FILE_TABLE[i].f_size;
        }
    }

    if((i == (NR_OF_FILES-1)) && !FILE_TABLE[i].in_use)
    {
        printf("No such file on disc\n");
        fclose(FS);
        return -1;
    }

    buffer = (char*)malloc(sizeof(char) * size);

    if(fseek(FS, addr, SEEK_SET) != 0)
    {
        printf("FSEEK failed\n");
        fclose(FS);
        return -1;
    }

    if(fread(buffer, sizeof(char) * size, 1, FS) != 1)
    {
        printf("Cannot read file from data segment\n");
        fclose(FS);
        return -1;
    }

    file = fopen(name, "w");

    if(fwrite(buffer, size, 1, file) != 1)
    {
        printf("Cannot copy file\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    free(buffer);

    printf("%s copied out\n", name);
    fclose(FS);
    fclose(file);

    return 0;
}

int copy_to_vd(const char* name)
{
    FILE *FS = NULL, *file = NULL;
    int size;
    int index;
    char *buffer;
    int i;

    file = fopen(name, "r");

    if(!file)
    {
        printf("File: %s doesnt exist\n", name);
        return -1;
    }

    if(fseek(file, 0, SEEK_END) != 0)
    {
        printf("FSEEK failed\n");
        fclose(file);
        return -1;
    }

    size = ftell(file);

    if(!(FS = fopen("vdisc", "r+w")))
    {
        printf("Cannot read disc file\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    rewind(FS);

    if(fread(&S_BLOCK, sizeof(S_BLOCK), 1, FS) != 1)
    {
        printf("Cannot read superblock\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    if(fread(FILE_TABLE, sizeof(FILE_TABLE), 1, FS) != 1)
    {
        printf("Cannot read FAT\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    for (i = 0; i < NR_OF_FILES; ++i)
    {
        if(!strcmp(FILE_TABLE[i].filename, name))
        {
            printf("File exist\n");
            return -1;
        }
    }


    if(S_BLOCK.files_count == NR_OF_FILES)
    {
        printf("Too many files on virtual disc\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    if(S_BLOCK.free_space < size)
    {
        printf("Too little memory\n");
        fclose(file);
        fclose(FS);
        return -1;
    }

    buffer = (char*)malloc(sizeof(char) * size);
    rewind(file);

    if(fread(buffer, sizeof(char) * size, 1, file) != 1)
    {
        printf("Cannot read file\n");
        fclose(FS);
        fclose(file);
        return -1;
    }

    fclose(file);

    index = S_BLOCK.first_to_be_used_index;

    strcpy(FILE_TABLE[index].filename, name);
    FILE_TABLE[index].f_size = size;
    FILE_TABLE[index].offset = S_BLOCK.free_addr;
    FILE_TABLE[index].in_use = 1;

    ++S_BLOCK.files_count;
    ++S_BLOCK.first_to_be_used_index;
    S_BLOCK.free_space -= size;
    S_BLOCK.free_addr += size;

    rewind(FS);

    if(!fwrite(&S_BLOCK, sizeof(S_BLOCK), 1, FS))
    {
        printf("Cannot update superblock\n");
        fclose(FS);
        return -1;
    }

    if(!fwrite(FILE_TABLE, sizeof(FILE_TABLE), 1, FS))
    {
        printf("Cannot update file table\n");
        fclose(FS);
        return -1;
    }

    rewind(FS);

    if(fseek(FS, FILE_TABLE[index].offset, SEEK_SET) != 0)
    {
        printf("FSEEK failed1\n");
        fclose(FS);
        return -1;
    }

    if(!fwrite(buffer, sizeof(char) * size, 1, FS))
    {
        printf("Cannot copy file\n");
        fclose(FS);
        return -1;
    }

    printf("%s copied in\n", name);
    free(buffer);
    fclose(FS);

    return 0;
}
