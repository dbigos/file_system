#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Filesystem.h"


int main(int argc, const char *argv[])
{
    if(argc < 2)
    {
        printf("Argument needed!\n");
        return -1;
    }

    if(!strcmp(argv[1],"create"))
    {
        if(argc < 3)
        {
            printf("Disc size needed!\n");
            return -1;
        }

        else
            return create_vd(atoi(argv[2]));
    }

    if(!strcmp(argv[1], "ls"))
        return ls();

    if(!strcmp(argv[1], "stat"))
        return statistics();

    if(!strcmp(argv[1], "cpout"))
    {
        if(argc < 3)
        {
            printf("Filename needed!\n");
            return -1;
        }

        else
            return copy_to_minix(argv[2]);
    }

    if(!strcmp(argv[1], "cpin"))
    {
        if(argc < 3)
        {
            printf("Filename needed!\n");
            return -1;
        }

        else
            return copy_to_vd(argv[2]);
    }

    if(!strcmp(argv[1], "rm"))
    {
        if(argc < 3)
        {
            printf("Filename needed!\n");
            return -1;
        }

        else
            return remove_file(argv[2]);
    }

    if(!strcmp(argv[1], "rmd"))
        return remove_vd();

    return 0;
}
