#!/bin/sh
mount /dev/c0d1 /mnt

cp -r /mnt/kernel /usr/src/
cp -r /mnt/mm /usr/src/
cp -r /mnt/fs /usr/src/
cp -r /mnt/include /usr/
cp -r /mnt/syslib/ /usr/src/lib/
cd /usr/src/tools
make clean 
make 
sync
umount /dev/c0d1
cd
cd /usr/src/tools
make hdboot
sync
