#!/bin/bash

cd /home/parallels/Developer/SOI/
sudo mount hdd.img minix_disk
sudo cp -r /media/psf/Home/Developer/minix/kernel minix_disk/
sudo cp -r /media/psf/Home/Developer/minix/mm minix_disk/
sudo cp -r /media/psf/Home/Developer/minix/include minix_disk/
sudo cp -r /media/psf/Home/Developer/minix/fs minix_disk/
sudo cp -r /media/psf/Home/Developer/minix/syslib minix_disk/
sleep 1s
sudo umount hdd.img
qemu-system-x86_64 minix.img -hdb hdd.img


