CC = gcc

all: 			fs

main.o: 		main.c
				$(CC) -c main.c

Filesystem.o:	Filesystem.c
				$(CC) -c Filesystem.c

fs: 			main.o  Filesystem.o 
				$(CC) main.o Filesystem.o -o fs

clean:
				rm *.o
				rm fs
