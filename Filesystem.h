#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUPER_BLOCK 		20			//sizeof
#define FAT_SIZE			28			//sizeof
#define NR_OF_FILES 		256
#define MINIMUM_FREE_SPACE 	1024
#define FIRST_FREE_SPACE 	7188

struct superblock
{
    int size;
    int files_count;
    int free_addr;
    int free_space;
    int first_to_be_used_index;
};


struct FAT
{
    char filename[16];
    int f_size;
    int offset;
    int in_use;
};


int create_vd(int);
int copy_to_vd(const char*);
int copy_to_minix(const char*);
int ls();
int remove_file(const char*);
int remove_vd();
int statistics();
int defragmentation();
